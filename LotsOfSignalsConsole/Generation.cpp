#define _USE_MATH_DEFINES
#include <math.h>
#include <chrono>
#include "Generation.h"

using namespace std::chrono;

void  SignalGenerator::GenerateSignal(SignalType type, SignalGenerationParameters params, Signal& dst)
{
	switch (type)
	{
	case BPSK:	return GenerateBPSKSignal(params, dst);
	case MSK:	return GenerateMSKSignal(params, dst);
	}
}

bool SignalGenerator::RandomBit(double low_chance)
{
	return (rand() < RAND_MAX * low_chance);
}

void SignalGenerator::GenerateBPSKSignal(SignalGenerationParameters params, Signal& ret_signal)
{
	ret_signal.clear();
	ret_signal.sampling = params.sampling_frequency;
	ret_signal.duration = params.duration;
	ret_signal.timestamp = params.start_timestamp;
	__int64 timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	ret_signal.name = string("BPSK") + to_string(timestamp);
	ret_signal.description = string("BPSK signal: ") +
		string("Sampling = ") + to_string(params.sampling_frequency) + string("; ") +
		string("Bitrate = ") + to_string(params.bitrate) + string("; ") +
		string("Start phase = ") + to_string(params.start_phase) + string("; ") +
		string("Timestamp = ") + to_string(params.start_timestamp) + string("; ") +
		string("Duration = ") + to_string(params.duration) + string("; ") +
		string("Add Parameter = ") + to_string(params.additional_parameter) + string(";");

	int samples_in_bit = (int)(params.sampling_frequency / params.bitrate);
	double sampling_period = 1. / params.sampling_frequency;

	double dbl_index = params.start_timestamp;
	while (dbl_index < params.start_timestamp + params.duration)
	{
		double bit = RandomBit(0.5);
		for (int n_sample = 0; n_sample < samples_in_bit; n_sample++)
		{
			ret_signal.signal.push_back(
				complex<double>(
					cos(params.start_phase + bit * M_PI),
					sin(params.start_phase + bit * M_PI)
					));
			ret_signal.keys.push_back(dbl_index);
			dbl_index += sampling_period;
		}
	}
}
void SignalGenerator::GenerateMSKSignal(SignalGenerationParameters params, Signal& ret_signal)
{
	ret_signal.clear();
	ret_signal.sampling = params.sampling_frequency;
	ret_signal.duration = params.duration;
	ret_signal.timestamp = params.start_timestamp;
	__int64 timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
	ret_signal.name = string("MSK") + to_string(timestamp);
	ret_signal.description = string("MSK signal: ") +
		string("Sampling = ") + to_string(params.sampling_frequency) + string("; ") +
		string("Bitrate = ") + to_string(params.bitrate) + string("; ") +
		string("Start phase = ") + to_string(params.start_phase) + string("; ") +
		string("Timestamp = ") + to_string(params.start_timestamp) + string("; ") +
		string("Duration = ") + to_string(params.duration) + string("; ") +
		string("Add Parameter = ") + to_string(params.additional_parameter) + string(";");

	double sampling_period = 1. / params.sampling_frequency;


	int samples_in_bit = (int)(params.sampling_frequency / params.bitrate);

	double separated_bit_summ = 0;
	double dbl_index = params.start_timestamp;
	double t = 0;
	while (dbl_index < params.start_timestamp + params.duration)
	{
		double separated_bit = RandomBit(0.5) * 2. - 1;
		for (int n_sample = 0; n_sample < samples_in_bit; n_sample++)
		{
			ret_signal.signal.push_back(
				complex<double>(
					cos(M_PI / 2. * (separated_bit_summ + params.bitrate * separated_bit * t)),
					sin(M_PI / 2. * (separated_bit_summ + params.bitrate * separated_bit * t))
					));
			ret_signal.keys.push_back(dbl_index);
			t += sampling_period;
			dbl_index += sampling_period;
		}
		separated_bit_summ += separated_bit;
	}
}

