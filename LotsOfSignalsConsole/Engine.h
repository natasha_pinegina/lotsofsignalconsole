#pragma once


#include "Methods.h"
#include "Generation.h"
#include <fstream>

class Researcher
{
public:
	int NumberOfSatellites=3;
	int NumberOfSources=4;
	vector<string> str;
	double KolOtch = 0;
	void DoResearch(int num_of_tests)
	{
		cout << "START" << endl;
		int positive_test_num = 0;
		int iter = 0;
		ofstream is("������������.txt");


		double KolPrav = 0;
		double KolOne = 0;
		double KolTwo = 0;

		vector<double> Polverout;
		Polverout.clear();
		vector<double> veroutminusone;
		veroutminusone.clear();
		vector<double> veroutminustwo;
		veroutminustwo.clear();

		for (double shum = -20; shum < 1; shum += 1)
		{
			cout << "SNR_dB = " << shum << endl;
			positive_test_num = 0;
			SNR_dB = shum;
			KolOtch = 0;
			KolPrav = 0;
			KolOne = 0;
			KolTwo = 0;
			for (int i = 0; i < num_of_tests; i++)
			{
				cout << "Try: " << i + 1 << "/" << num_of_tests << endl;
				GenerateSignalsAndCorrelations(BPSK);
				//GenerateTwoLongSignal();
				if (KolOtch == NumberOfSources) KolPrav++;
				if (KolOtch == NumberOfSources - 1 || KolOtch == NumberOfSources) KolOne++;
				if (KolOtch == NumberOfSources - 2 || KolOtch == NumberOfSources - 1 || KolOtch == NumberOfSources) KolTwo++;
			}
			Polverout.push_back(KolPrav / num_of_tests);
			veroutminusone.push_back(KolOne / num_of_tests);
			veroutminustwo.push_back(KolTwo / num_of_tests);
			is << Polverout[iter] << "\t" << veroutminusone[iter] << "\t" << veroutminustwo[iter] << "\n";
			iter++;
		}
		cout << "FINISH" << endl;
		is.close(); // ��������� ����
	}
private:
	void GenerateSignalsAndCorrelations(SignalType signal_type)
	{
		vector<Signal> massLongSignal(NumberOfSources);
		/*massLongSignal.clear();
		massLongSignal.resize(NumberOfSources);*/
		SignalGenerationParameters parametrs = { startTimestamp,startPhase,Duration,nSamples,
			samplingFrequency,Bitrate,additionalParameter };

		for (int i = 0; i < NumberOfSources; i++)
		{
			SignalGenerator ss;
			ss.GenerateSignal(signal_type, parametrs, massLongSignal[i]);
			parametrs = { startTimestamp,startPhase,Duration,nSamples,
				samplingFrequency,Bitrate,additionalParameter };
		}

		vector<vector<Signal>> massSignal(NumberOfSatellites, vector<Signal>(NumberOfSources));
		//massSignal.clear();

		vector<Signal> SummSignal(NumberOfSatellites);
		/*SummSignal.clear();
		SummSignal.resize(NumberOfSatellites);*/

		vector<double> sdvig;
		sdvig.clear();

		sdvig = Delays(NumberOfSources, NumberOfSatellites);

		int k = 0;
		double d = Duration / 10;
		for (int i = 0; i < NumberOfSatellites; i++)
		{
			for (int j = 0; j < NumberOfSources; j++)
			{
				Methods::transformSignal(massLongSignal[j], sdvig[i + NumberOfSatellites * j] * d, d, 0, 1, SNR_dB, massSignal[i][j]);
			}
			for (int j = 1; j < NumberOfSources; j++)
			{
				if (j == 1)	Methods::coherentSumm({ massSignal[i][0] , massSignal[i][j] }, SummSignal[i]);
				else		Methods::coherentSumm({ SummSignal[i] , massSignal[i][j] }, SummSignal[i]);
			}
		}

		vector<Signal> correlate(NumberOfSatellites);
		for (int i = 0; i < NumberOfSatellites; i++)
		{
			if (i + 1 == NumberOfSatellites) Methods::Newcorrelate(SummSignal[i], SummSignal[0], correlate[i]);
			else Methods::Newcorrelate(SummSignal[i], SummSignal[i + 1], correlate[i]);
		}

		double clearance = 1.0 / Bitrate * samplingFrequency;
		vector<vector<int>> delays;
		delays.clear();
		delays.resize(NumberOfSatellites);
		for (int i = 0; i < NumberOfSatellites; i++)
		{
			auto delays1 = find_max_n(NumberOfSources, correlate[i], clearance);
			//auto delays1 = find_max_n(KolMax, correlate[i], clearance);
			delays[i] = delays1;
		}

		vector<double> SummDelays;
		SummDelays.clear();
		SummDelays = criteria(delays);
		KolOtch = SummDelays.size();
	}

	vector<int> find_max_n(int n, Signal &sig, double clearance)
	{
		if (sig.signal.empty()) return {};
		vector<double> abs_values(sig.signal.size());
		for (int i = 0; i < abs_values.size(); i++)
		{
			abs_values[i] = abs(sig.signal[i]);
		}

		vector <int> max_inds;
		for (int max_idx = 0; max_idx < n; max_idx++)
		{
			int max_ind = 0;
			for (int i = 1; i < sig.signal.size() - 1; i++)
			{
				if (abs_values[i - 1] < abs_values[i] && abs_values[i + 1] < abs_values[i])
				{
					int peak_index = i;
					bool unique_peak = true;
					for (int j = 0; j < max_inds.size(); j++)
					{
						unique_peak &= abs(peak_index - max_inds[j]) > clearance;	//&& max_inds[j] != 0;
					}
					if ((abs_values[peak_index] > abs_values[max_ind]) && unique_peak)
					{
						max_ind = i;
					}
				}
			}
			max_inds.push_back(max_ind);
		}
		sort(max_inds.begin(), max_inds.end());
		for (auto& ind : max_inds)
		{
			ind = sig.keys[ind];
		}

		return max_inds;
	}


	vector<double> criteria(vector<vector<int>> delays)
	{
		double clearance = 1.0 / Bitrate * samplingFrequency;
		int summ = 0;
		int s = 0;
		int buf = 0;
		vector<double> SummDelays;
		SummDelays.clear();
		for (int i = 0; i < delays[0].size(); i++)// �� ��������� ������ ������
		{
			summ = delays[0][i];
			s = 1;
			for (int k = 0; k < delays[0].size(); k++)//�� �������
			{
				summ += delays[s][k];
				int iter = 0;
				buf = summ;
				for (int m = 0; m < delays[0].size(); m++)// �� ��������
				{
					summ += delays[s + 1][m];
					if (abs(summ) <= clearance)
					{
						SummDelays.push_back(summ);
						string stroka = to_string(i) + to_string(k) + to_string(m);
						str.push_back(stroka);
					}
					summ = buf;
				}
				summ = delays[0][i];

			}

		}
		return SummDelays;
	}

	vector<double> Delays(int NumberOfSources, int NumberOfSatellites)
	{
		vector<double> del;
		for (int i = 0; i < NumberOfSources; i++)
		{
			for (int j = 0; j < NumberOfSatellites; j++)
			{
				del.push_back(1. + 0.5 * ((double)rand() / RAND_MAX - 0.5));
			}
		}

		return del;
	}

	
	
private:
	//������� �������������
	double samplingFrequency = 300e6;
	//����� ������� ������
	double startTimestamp = 0;
	//�����������������
	double Duration = 1e-3;
	//��������� ����
	double startPhase = 0;
	double nSamples = 0;
	//�������� �������� ������
	double Bitrate = 100e5;
	//�������������� ��������
	double additionalParameter = 0;

	double SNR_dB;
};