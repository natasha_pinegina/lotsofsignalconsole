#pragma once
#include <vector>
#include <complex>


using namespace std;

enum SignalType
{
	//�������� ������� ���������
	BPSK,
	//������������������ ������� �
	QPSK,
	//��������� ���������
	MSK,
	//������������������� � ������������� ��������� ����������� �������
	OFDM,
	//��������������� ����������� ������� �������
	FHSS,
};

struct Signal
{
	vector<complex<double>> signal;
	vector<double> keys;

	string name;
	//��������
	string description;

	double sampling;
	double timestamp;
	double duration;

	void clear()
	{
		vector<complex<double>>().swap(signal);
		vector<double>().swap(keys);
		string().swap(name);
		string().swap(description);
	}

};